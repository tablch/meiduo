import re

from django_redis import get_redis_connection
from rest_framework import serializers
from . models import User
from rest_framework_jwt.settings import api_settings


class UserSerializer(serializers.ModelSerializer):
    """额外3个字段 需要显示指明  password2  sms_code  allow """
    password2 = serializers.CharField(label='确认密码', max_length=20, min_length=8, write_only=True)
    sms_code = serializers.CharField(label='短信验证码', max_length=6, min_length=6, write_only=True)
    allow = serializers.CharField(label='同意协议', write_only=True)
    token = serializers.CharField(read_only=True)

    class Meta:
        model = User
        fields = ('username', 'password', 'password2', 'mobile', 'sms_code', 'allow', 'token')

        extra_kwargs = {
            'username': {
                "max_length": 20,
                "min_length": 5,
                "error_messages": {
                    "max_length": "名字过长",
                    "min_length": "名字过短"
                }
            },
            'password': {
                "write_only": True,
                "max_length": 20,
                "min_length": 8,
                "error_messages": {
                    "max_length": "密码过长",
                    "min_length": "密码过短"
                }
            },
        }

    def validate_mobile(self, value):
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号错误')
        return value

    def validate_allow(self, value):
        if value != 'true':
            raise serializers.ValidationError('请同意协议')
        return value

    def validate(self, attrs):
        # 两次密码
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError('两次密码不一致')
        redis_conn = get_redis_connection('verify')
        mobile = attrs['mobile']
        real_sms_code = redis_conn.get('sms_code_%s' % mobile)
        if not real_sms_code:
            raise serializers.ValidationError('短信验证码过期')

        if attrs['sms_code'] != real_sms_code.decode():
            raise  serializers.ValidationError('短信验证码填写错误')
        return attrs

    def create(self, validated_data):
        print(validated_data)
        del validated_data['allow']
        del validated_data['sms_code']
        del validated_data['password2']
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()

        # 额外生成一个登录状态的token值 这四条写法时固定的  只需修要荷载的对象
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        user.token = token
        return user


